from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('blog/', views.blog, name='blog'),
    path('search/', views.search, name="search"),
    path('blog/<slug:slug>/', views.post_detail, name="post_detail_url"),
    path('blog/<slug:slug>/leave_comment/', views.comment, name='comment'),
    path('category/<slug:slug>/', views.category_detail, name="category_detail_url"),
    path('register/', views.register, name="register"),
    path('send-feedback/', views.feedback, name="feedback"),

    
]
