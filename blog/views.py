from django.shortcuts import render, redirect, reverse
from django.http import Http404
from .models import Post, Category, Comment, FeedBack   
from django.db.models import Q
from .forms import RegisterForm
from django.contrib.auth.models import User

def index(request):
    return render(request, 'blog/index.html')

def blog(request):
    news = Post.objects.order_by('-date')
    return render(request, 'blog/blog.html', {'news':news})

def search(request):
    query = request.GET.get('search')
    search_object = Post.objects.filter(
        Q(title__icontains = query) | Q(summary__icontains = query)
    )
    context = {'query': query, 'search_obj': search_object}
    return render(request, 'blog/search.html', context)

def post_detail(request, slug):
    post = Post.objects.get(slug__exact = slug)
    return render(request, 'blog/post_detail.html', {'post':post})

def category_detail(request, slug):
    category = Category.objects.get(slug__exact = slug)
    return render(request, 'blog/category_detail.html', {'category':category})

def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = RegisterForm()
    return render(request, 'blog/register.html', {'form':form})

def comment(request, slug):
    try:
        post = Post.objects.get(slug__exact = slug)
    except:
        raise Http404('Статья не найдена')
    if request.user.is_authenticated:
        user = request.user.first_name
        post.comment_set.create(
            author_name = user, text = request.POST.get('text')
        )
    else:
        post.comment_set.create(
            author_name = request.POST.get('name'), text = request.POST.get('text')
        )
    return redirect(reverse('post_detail_url', args = (slug,)))

def feedback(request):
    mail = FeedBack.objects.all()
    mail.create(
        name = request.POST.get('name'),
        phone = request.POST.get('phone'),
        email = request.POST.get('email'),
        text = request.POST.get('text')
    )
    return redirect('index')
# Create your views here.
